import os
import sys
import socket


def send_file_bytes(host, file_name):
    port = 50001
    _socket = socket.socket()
    _socket.connect((host, 65535))
    f = open(f'files_to_distribute/{file}', "rb")
    print(f'Sending file name {file_name}')
    _socket.send(file_name.encode('utf-8'))
    msg = _socket.recv(1024).decode('utf-8')
    chunk_read = f.read(1024)
    while chunk_read:
        #print('Sending chunk of data')
        _socket.send(chunk_read)
        chunk_read = f.read(1024)
    _socket.close()


number_of_nodes = int(sys.argv[1])
print(f'Number of nodes {number_of_nodes}')
nodes_ip_addresses = []

for node_i in range(0, number_of_nodes):
    print(f'Appending node ip {node_i}')
    nodes_ip_addresses.append(sys.argv[2+node_i])

print(nodes_ip_addresses)

# Remove gitkeep
files = os.listdir('files_to_distribute')
for file in files:
    if file == '.gitkeep':
        os.remove(f'files_to_distribute/{file}')

files = os.listdir('files_to_distribute')
nr_of_files = len(files)
print(f'Total number of files {nr_of_files}')
nr_of_extra_files = nr_of_files % number_of_nodes
print(f'Nr of extra  {nr_of_extra_files}')

nr_of_files_per_node = nr_of_files / number_of_nodes
nr_of_files_per_node = int(nr_of_files_per_node)

print(f'nr_of_files_per_node {nr_of_files_per_node}')
current_node = 0

while current_node < number_of_nodes:
    # Connect to current node
    current_file_number = 0
    files = os.listdir('files_to_distribute')
    for file in files:
        if current_file_number < nr_of_files_per_node:
            # Send file to current node
            current_node_ip = nodes_ip_addresses[current_node]
            print(f'Sending file {file} to node {current_node_ip}')
            send_file_bytes(host=current_node_ip, file_name=file)
            os.remove(f'files_to_distribute/{file}')
            current_file_number += 1
        else:
            #print(f'Sent all files to {current_node_ip}')
            break
    current_node += 1

current_node = 0
# Handle extra files
nr_of_files_per_node = 1
while current_node < number_of_nodes:
    # Connect to current node
    current_file_number = 0
    files = os.listdir('files_to_distribute')
    for file in files:
        if current_file_number < nr_of_files_per_node:
            # Send file to current node
            current_node_ip = nodes_ip_addresses[current_node]
            print(f'Sending extra file {file} to node {current_node_ip}')
            send_file_bytes(host=current_node_ip, file_name=file)
            os.remove(f'files_to_distribute/{file}')
            current_file_number += 1
        else:
            break
    current_node += 1
