#!/usr/bin/env python
# Checking changes
import pika, sys, os, time, shutil, subprocess, threading, json, base64, filter_types, sys
from ilock import ILock, ILockException
from datetime import datetime
import write_timestamp

import get_queue_size

# datetime object containing current date and time


def ffmpeg_running():

    #return False

    name = 'ffmpeg'
    for line in os.popen("ps ax | grep " + name + " | grep -v grep"):
        return True
    return False


def _process(filename, task_type):
    if task_type == filter_types.BLUR:
        # os.system(f'ffmpeg -i "{filename}" \-filter_complex "[0:v]crop=200:400:300:350,boxblur=10[fg]; [0:v][fg]overlay=300:350[v]" \-map "[v]"  \ blurredVideo.mp4')
        time.sleep(filter_types.BLUR_PROCESSING_TIME)
    elif task_type == filter_types.STABILIZE:
        # Replace with actual encoding
        # os.system(f'ffmpeg -i "{filename}" \-filter_complex "[0:v]crop=200:400:300:350,boxblur=10[fg]; [0:v][fg]overlay=300:350[v]" \-map "[v]"  \ blurredVideo.mp4')
        time.sleep(10)
    elif task_type == filter_types.MERGE_PARTS:
        # Replace with actual encoding
        # os.system(f'ffmpeg -i "{filename}" \-filter_complex "[0:v]crop=200:400:300:350,boxblur=10[fg]; [0:v][fg]overlay=300:350[v]" \-map "[v]"  \ blurredVideo.mp4')
        time.sleep(filter_types.MERGE_PARTS_PROCESSING_TIME)
    os.system(f'touch blurredVideo_{filename}.mp4')
    # time.sleep(random.randint(10, 20))
    shutil.move(f'blurredVideo_{filename}.mp4', 'producer_folder/' + filename)


def _run_ffmpeg_command(output_folder_name, f, dt_string, filename, ffmpeg_command):
    producer_folder_name = 'producer_folder'

    if output_folder_name:
        producer_folder_name = output_folder_name
    now = datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    f.write(f'{dt_string} PREPARE RUN FFMPEG {filename}\n')
    f.flush()

    count = 0
    while count < 60:
        write_timestamp.write_time_stamp()
        count += 1
        time.sleep(1)

    shutil.move(f'{filename}', f'{producer_folder_name}/' + filename)
    now = datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    f.write(f'{dt_string} MOVED FILE {filename} to {producer_folder_name}\n')
    f.flush()


def run_ffmpeg_command(output_folder_name, f, dt_string, filename, ffmpeg_command):
    producer_folder_name = 'producer_folder'

    if output_folder_name:
        producer_folder_name = output_folder_name
    now = datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    f.write(f'{dt_string} PREPARE RUN FFMPEG {filename}\n')
    f.flush()
    while True:
        status = os.system(
            f'ffmpeg -i "{filename}" \-filter_complex "[0:v]crop=200:400:300:350,boxblur=10[fg]; [0:v][fg]overlay=300:350[v]" \-map "[v]"  \ blurredVideo_{filename}.mp4 -y')
        now = datetime.now()
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
        f.write(f'{dt_string} OS RETURNED {status} {filename}\n')
        f.flush()
        if status == 0:
            break
        else:
            try:
                os.remove(f' blurredVideo_{filename}.mp4')
            except:
                pass
            time.sleep(2)

    shutil.move(f' blurredVideo_{filename}.mp4', f'{producer_folder_name}/' + filename)
    now = datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    f.write(f'{dt_string} MOVED FILE {filename} to {producer_folder_name}\n')
    f.flush()


def process(filename, task_type, output_folder_name):
    # print(f'Processing filename: {filename}')
    time.sleep(5)
    now = datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")

    f = open('consumer_output.txt', 'a')

    if task_type == filter_types.BLUR:
        run_ffmpeg_command(output_folder_name,f, dt_string, filename, f'ffmpeg -i "{filename}" \-filter_complex "[0:v]crop=200:400:300:350,boxblur=10[fg]; [0:v][fg]overlay=300:350[v]" \-map "[v]"  \ blurredVideo_{filename}.mp4 -y')
    elif task_type == filter_types.STABILIZE:
        run_ffmpeg_command(output_folder_name, f, dt_string, filename, f'ffmpeg -i "{filename}" -vf deshake " blurredVideo_{filename}.mp4" -y')
    elif task_type == filter_types.MERGE_PARTS:
        f.write(f'{dt_string} PREPARE MOVE {filename}\n')
        f.flush()
        shutil.move(filename, 'producer_folder/' + filename)
        f.write(f'{dt_string} MOVED FILE {filename}\n')
        f.flush()
    elif task_type == filter_types.ADD_WATERMARK:
        run_ffmpeg_command(output_folder_name, f, dt_string, filename, f"ffmpeg -i {filename} -i open-source.png -filter_complex 'overlay=10:main_h-overlay_h-10' ' blurredVideo_{filename}.mp4' -y")
    elif task_type == filter_types.REVERSE:
        run_ffmpeg_command(output_folder_name, f, dt_string, filename, f"ffmpeg -i {filename} -vf reverse -af areverse ' blurredVideo_{filename}.mp4' -y")
    elif task_type == filter_types.VINTAGE:
        run_ffmpeg_command(output_folder_name, f, dt_string, filename, f"ffmpeg -i {filename} -vf curves=vintage ' blurredVideo_{filename}.mp4' -y")
    f.close()


def get_video_length(filename):
    result = subprocess.run(["ffprobe", "-v", "error", "-show_entries",
                             "format=duration", "-of",
                             "default=noprint_wrappers=1:nokey=1", filename],
                            stdout=subprocess.PIPE,
                            stderr=subprocess.STDOUT)
    return float(result.stdout)


def split_video(filename):
    # splits video in multiple parts and saves them in output folder to be read by the producer
    # print(f'Splitting video {filename}')
    # sas split only half the vier
    # save output to producer folder
    # shutil.move(os.curdir+'/'+filename, '../producer_folder/'+filename)=
    os.system(
        f'../split_video.sh {filename} 10000000 "-c:v libx264 -crf 23 -c:a copy -vf scale=960:-1"')  # split in 30 Megabytes parts
    os.system('cp -r part-* ../producer_folder')
    """
    Split in 60 seconds slices
    :param filename:
    :return:
    v_len = get_video_length(filename)
    v_len_int = int(v_len)
    v_len_int = v_len_int / 60  # minutes
    # We want to split in chunks of 2 minutes each
    number_of_chunks = v_len_int
    number_of_chunks = int(round(number_of_chunks))
    # print(f'Number of chunks {number_of_chunks}')
    # print(f'vid len: {v_len}')
    start = 0
    end = 0
    for chunk in range(1, number_of_chunks + 1):
        # #print(f'------start:{start} end:{end}')
        start = end
        end += 60
        if end > v_len:
            end = v_len
        # print(f'Splitting {start}:{end}')
        part_name = f'part_{chunk}' + '_of_' + str(number_of_chunks) + '_startFilename' + filename
        os.system(f'ffmpeg -i "{filename}" -ss {start} -t 60 "{part_name}"')
        shutil.move(part_name, '../producer_folder/' + part_name)
    """

def track_time(state, file_name, task_type):
    tracker_file = open('track_time.txt', 'a')
    tracker_file.write(f"{state} > {get_now_time()} > {file_name} > {task_type} \n")
    tracker_file.flush()
    tracker_file.close()

def get_now_time():
    now = datetime.now()

    # dd/mm/YY H:M:S
    dt_string = now.strftime("%H:%M:%S")
    return dt_string

def log_message(msg):
    now = datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    f = open('consumer_output.txt', 'a')
    f.write(f'{dt_string}  {msg}\n')
    f.flush()

def consuming_from_another_node(q_ip):
    f = open("id.txt", 'r')
    id = f.read()
    id = str(int(id))
    f.close()
    ip = f'172.16.0.{id}'
    return q_ip != ip


def decrease_q_size():
    f = open('first_q_size.txt', 'r')
    q_size = f.read()
    q_size = int(q_size)
    q_size -= 1
    f.close()
    f = open('first_q_size.txt', 'w')
    f.write(str(q_size))
    f.close()


def all_queues_are_empty():
    q_sizes = get_queue_size.queue_size_all(['hello', 'hello2', 'hello3', 'hello4'])
    sum = 0
    for q_size in q_sizes:
        sum += int(q_size)
    return sum == 0

def main(task_type, queue_ip, third_argument, forth_argument):
    output_folder_name = None
    queue_name = 'hello'
    if third_argument and filter_types.SECOND_PRODUCER_FOLDER_NAME in third_argument:
        output_folder_name = third_argument
        queue_name = forth_argument
    elif third_argument:
        # provided a new queue name
        queue_name = third_argument

    if task_type == filter_types.SPLIT:
        # Only executed in first node from pipeline
        # print('SPLIT command, means we are first node in pipeline')
        os.chdir('consumer_folder')
        # yt_download.download_videos()
        files = os.listdir()

        for file in files:
            split_video(file)
            # os.remove(file)
    else:
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(host=queue_ip, credentials=pika.PlainCredentials('adi', '24adna')))  # loopback adress of above node
        channel = connection.channel()

        channel.queue_declare(queue=queue_name)

        def callback(ch, method, properties, body):
            # print('Callback launched')
            d = json.loads(body.decode())
            file_name = d.pop('file_name')

            if file_name == 'END_OF_CONN':
                ch.basic_ack(delivery_tag=method.delivery_tag)
                exit(1)

            try:
                # Controller gracefully stop consumer after its done processing the previous file
                f = open('stop_further_processing', 'r')
                f.close()
                os.remove('stop_further_processing')
                ch.basic_nack(delivery_tag=method.delivery_tag, multiple=False, requeue=True)
                # os.system('python3 kill_process.py ')
                exit(1)
            except FileNotFoundError:
                pass
            processed_file = False
            try:
                with ILock('consumer_lock', timeout=3):
                    # The code should be run as a system-wide single instance
                    if not ffmpeg_running():
                        if True:#(consuming_from_another_node(queue_ip) and all_queues_are_empty()) or not consuming_from_another_node(queue_ip):
                            ch.basic_ack(delivery_tag=method.delivery_tag)
                            if queue_name == 'first':
                                with ILock('q_update_lock', timeout=100):
                                    decrease_q_size()
                            # If we are consuming from another node, all our queues must be empty or we can keep consuming from this node
                            # We want to consume from another node if all our queues are empty
                            # Otherwise, nack this message
                            # Problem: messages remain in queue until they are finished processing

                            track_time('START', file_name, task_type)

                            data = d.pop('binary')
                            data = base64.b64decode(data)

                            f = open(file_name, 'wb')
                            f.write(data)
                            f.close()

                            f = open('locked_flag', 'w')
                            f.write('da')
                            f.close()

                            now = datetime.now()

                            # dd/mm/YY H:M:S
                            dt_string = now.strftime("%d/%m/%Y %H:%M:%S")

                            f = open('consumer_output.txt', 'a')

                            f.write(f'{dt_string}   Wrote file {file_name} {task_type} from {queue_ip} {queue_name} and will put output in {output_folder_name}\n')

                            f.flush()

                            f.close()
                            processThread = threading.Thread(target=process, args=(file_name, task_type, output_folder_name,))
                            processThread.start()
                            processThread.join()
                            # process(file_name, task_type)
                            # print(' [x] Done processing')
                            f = open('consumer_output.txt', 'a')
                            try:
                                os.remove(file_name)
                                f.write(f'{dt_string}  Removed {file_name}\n')
                                f.flush()
                            except:
                                pass
                            # print('Removed local file')

                            f.write(f'{dt_string}   ACK {file_name}\n')
                            f.flush()
                            f.write(f'==============================================\n')
                            f.close()

                            track_time('END', file_name, task_type)
                            os.remove('locked_flag')

                            # f = open('consumer_output.txt', 'a')
                            # if queue_name != 'hello5':
                            #     if queue_name != 'hello4':
                            #         f.write(f'FINISHED PROCESSING {file_name} FROM {queue_name}, KILLING MYSELF and checker_daemon\n')
                            #         os.system(f'python3 kill_process.py checker_daemon.py')
                            #         os._exit(1)
                            #     else:
                            #         f.write(
                            #             f'FINISHED PROCESSING {file_name} FROM {queue_name}, START CHECKER DAEMON BACK\n')
                            #         os.system(f'python3 checker_daemon.py&')
                            #         f.write(
                            #             f'DONE LAUNCHING CHECKER DAEMON\n')
                            # processed_file = True
                            # print('Sent ack')
                        else:
                            # Consume your own messages first
                            ch.basic_nack(delivery_tag=method.delivery_tag, multiple=False, requeue=True)
                    else:
                        # print('Rejected message because ffmpeg is running')
                        ch.basic_nack(delivery_tag=method.delivery_tag, multiple=False, requeue=True)
                        # time.sleep(30)
            except ILockException:
                ch.basic_nack(delivery_tag=method.delivery_tag, multiple=False, requeue=True)
                # exit
                # restart process when
                os._exit(1)
            # if processed_file:
            #     # Wait until the file reached the last filter
            #     time.sleep(100)
            #     f = open('consumer_output.txt', 'a')
            #     f.write(
            #         f'WAITING {file_name} from {queue_ip} {queue_name} TO REACH END\n')
            #     while True:
            #
            #         f.flush()
            #         sum = 0
            #         retry_count = 0
            #         while retry_count < 2:
            #             q_sizes = get_queue_size.queue_size_all(['hello', 'hello2', 'hello3', 'hello4', 'hello5'])
            #             f.write(f'{dt_string} Q SIZES {q_sizes}\n')
            #             f.flush()
            #             for q_size in q_sizes:
            #                 f.write(f'{dt_string} CURRENT Q SIZE {q_size} \n')
            #                 f.flush()
            #                 sum += int(q_size)
            #             if sum == 0:
            #                 retry_count += 1
            #                 time.sleep(5)
            #             else:
            #                 break
            #
            #         if sum == 0:
            #             f.write(f'{dt_string} Q SIZE IS 0, FILE REACH END \n')
            #             f.flush()
            #             f.close()
            #             break
            #         else:
            #             # Thare are still files in the pipeline, wait for other consumer processes to apply the filters
            #             f.write(f'Q SIZE {q_size} is not zero, waiting \n')
            #             f.flush()
            #             time.sleep(10)



        channel.basic_qos(prefetch_count=1)
        channel.basic_consume(queue=queue_name, on_message_callback=callback)

        print(' [*] Waiting for messages. To exit press CTRL+C')
        channel.start_consuming()


if __name__ == '__main__':
    third_argument = None
    forth_argument = None
    try:
        third_argument = sys.argv[3]
    except IndexError:
        # argument has not been provided
        pass
    try:
        forth_argument = sys.argv[4]
    except IndexError:
        # argument has not been provided
        pass
    try:
        task_type = sys.argv[1]
        queue_ip = sys.argv[2]
        main(task_type, queue_ip, third_argument, forth_argument)
    except Exception as e:
        print(f'EXCEPTION OCCURED {e}')
        f = open('consumer_output.txt', 'a')

        f.write(
            f'EXCEPTION OCCURED {e}\n')

        f.flush()

        f.close()
        time.sleep(1)
