import os
import sys
import socket
import cv2

def get_file():
    files = os.listdir('files_to_distribute')
    for file in files:
        if file != '.gitkeep':
            return file
    return None


def update_node_frames(key, frames):
    nodes_frames_dictionary[key] += frames
    print(f'UPDATE After update node {key} has {nodes_frames_dictionary[key]} frames')


def get_node_with_least_frames():
    lowest_frames = 100000000
    lowest_frames_node = None
    for key in nodes_frames_dictionary:
        if nodes_frames_dictionary[key] < lowest_frames:
            lowest_frames_node = key
            lowest_frames = nodes_frames_dictionary[key]
    print(f'GET Node {lowest_frames_node} has {lowest_frames} frames')
    return lowest_frames_node

def get_file_with_smallest_frame_nr():
    smallest_frame_nr = 100000000
    smallest_file_name = None
    files = os.listdir('files_to_distribute')
    for file in files:
        if file != '.gitkeep':
            cap = cv2.VideoCapture(f'files_to_distribute/{file}')
            length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

            if length < smallest_frame_nr:
                smallest_file_name = file
                smallest_frame_nr = length
    return smallest_file_name


def get_file_with_largest_frame_nr():
    largest_frame_nr = 0
    largest_file_name = None
    files = os.listdir('files_to_distribute')
    for file in files:
        if file != '.gitkeep':
            cap = cv2.VideoCapture(f'files_to_distribute/{file}')
            length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

            if length > largest_frame_nr:
                largest_file_name = file
                largest_frame_nr = length
    return largest_file_name, largest_frame_nr

def send_file_bytes(host, file_name):
    port = 50001
    _socket = socket.socket()
    _socket.connect((host, 65535))
    f = open(f'files_to_distribute/{file}', "rb")
    print(f'Sending file name {file_name}')
    _socket.send(file_name.encode('utf-8'))
    msg = _socket.recv(1024).decode('utf-8')
    chunk_read = f.read(1024)
    while chunk_read:
        #print('Sending chunk of data')
        _socket.send(chunk_read)
        chunk_read = f.read(1024)
    _socket.close()


number_of_nodes = int(sys.argv[1])
print(f'Number of nodes {number_of_nodes}')
nodes_ip_addresses = []

for node_i in range(0, number_of_nodes):
    print(f'Appending node ip {node_i}')
    nodes_ip_addresses.append(sys.argv[2+node_i])

print(nodes_ip_addresses)

# Remove gitkeep
files = os.listdir('files_to_distribute')
for file in files:
    if file == '.gitkeep':
        os.remove(f'files_to_distribute/{file}')

files = os.listdir('files_to_distribute')
nr_of_files = len(files)
print(f'Total number of files {nr_of_files}')
nr_of_extra_files = nr_of_files % number_of_nodes
print(f'Nr of extra  {nr_of_extra_files}')

nr_of_files_per_node = nr_of_files / number_of_nodes
nr_of_files_per_node = int(nr_of_files_per_node)

print(f'nr_of_files_per_node {nr_of_files_per_node}')
current_node = 0

current_file_number = 0
first_file_of_each_node_distribution = True

nodes_frames_dictionary = {}

# initiate dictionary
for node_ip in nodes_ip_addresses:
    nodes_frames_dictionary[node_ip] = 0


# get node with least frames
# get file with largest frames
while True:
    ip = get_node_with_least_frames()
    print(f'Got node with least frames {ip}')
    file, frames = get_file_with_largest_frame_nr()
    print(f'Got file {file} with largest frames {frames}')
    update_node_frames(ip, frames)
    if file:
        #current_node_ip = nodes_ip_addresses[current_node]
        print(f'Sending file {file} to node {ip}')
        send_file_bytes(host=ip, file_name=file)
        os.remove(f'files_to_distribute/{file}')
    else:
        print('Out of files')
        break

# while current_file_number < nr_of_files_per_node:
#     print(f'Sending file number {current_file_number} to all nodes')
#     current_node = 0
#
#     while current_node < number_of_nodes:
#         file = None
#         if first_file_of_each_node_distribution:
#             print('Sending largest file')
#             file = get_file_with_largest_frame_nr()
#         else:
#             print('Sending smallest file')
#             file = get_file_with_smallest_frame_nr()
#         print(f'Prepare send file {file} to node {current_node}')
#         if file:
#             current_node_ip = nodes_ip_addresses[current_node]
#             print(f'Sending file {file} to node {current_node_ip}')
#             send_file_bytes(host=current_node_ip, file_name=file)
#             os.remove(f'files_to_distribute/{file}')
#         else:
#             break
#         # Reached the last node
#         current_node += 1
#     first_file_of_each_node_distribution = False
#     # Reached the last file per node
#     current_file_number += 1

# while current_node < number_of_nodes:
#     # Connect to current node
#     current_file_number = 0
#     #files = os.listdir('files_to_distribute')
#     while True:
#         file = get_file()
#         if file:
#             if current_file_number < nr_of_files_per_node:
#                 # Send file to current node
#                 current_node_ip = nodes_ip_addresses[current_node]
#                 print(f'Sending file {file} to node {current_node_ip}')
#                 send_file_bytes(host=current_node_ip, file_name=file)
#                 os.remove(f'files_to_distribute/{file}')
#                 current_file_number += 1
#             else:
#                 #print(f'Sent all files to {current_node_ip}')
#                 break
#         else:
#             break
#     current_node += 1

current_node = number_of_nodes - 1
# Handle extra files
nr_of_files_per_node = 1

current_file_number = 0

# while current_file_number < nr_of_files_per_node:
#     print(f'Sending EXTRA file number {current_file_number} to all nodes')
#     current_node = number_of_nodes - 1
#     while current_node >= 0:
#         file = get_file_with_smallest_frame_nr()
#         print(f'Prepare send EXTRA file {file} to node {current_node}')
#         if file:
#             current_node_ip = nodes_ip_addresses[current_node]
#             print(f'Sending EXTRA file {file} to node {current_node_ip}')
#             send_file_bytes(host=current_node_ip, file_name=file)
#             os.remove(f'files_to_distribute/{file}')
#         else:
#             break
#         # Reached the last node
#         current_node -= 1
#     # Reached the last file per node
#     current_file_number += 1

# while current_node < number_of_nodes:
#     # Connect to current node
#     current_file_number = 0
#     #files = os.listdir('files_to_distribute')
#     #for file in files:
#     while True:
#         file = get_file()
#         if file:
#             if current_file_number < nr_of_files_per_node:
#                 # Send file to current node
#                 current_node_ip = nodes_ip_addresses[current_node]
#                 print(f'Sending extra file {file} to node {current_node_ip}')
#                 send_file_bytes(host=current_node_ip, file_name=file)
#                 os.remove(f'files_to_distribute/{file}')
#                 current_file_number += 1
#             else:
#                 break
#         else:
#             break
#     current_node += 1
