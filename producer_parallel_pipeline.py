#!/usr/bin/env python
import pika, sys, os, time, json, base64, psutil
import threading
from datetime import datetime

import filter_types

QUEUE_NAME = 'wqueue'

def get_now_time():
    now = datetime.now()

    # dd/mm/YY H:M:S
    dt_string = now.strftime("%H:%M:%S")
    return dt_string

def enough_memory():
    mem = psutil.virtual_memory()
    if mem.percent >= 80:
        return False
    return True


def get_q_size(channel, queue_name):
    while True:
        f = open(f'../pika_q_size_{queue_name}.txt', 'w')
        res = channel.queue_declare(queue=queue_name, passive=True)
        #print(f'Messages in queue {queue_name}: {res.method.message_count}')
        f.write(f'{res.method.message_count}')
        f.close()
        time.sleep(3)

def work(host, second_argument, third_argument):
    working_directory = 'producer_folder'
    queue_name = 'hello'
    if second_argument and third_argument:
        txt.write(f'Provided both arguments, using queue name {second_argument} and uploading from {third_argument}')
        txt.flush()
        # Second queue name provided, use second producer folder
        working_directory = third_argument
        queue_name = second_argument

    os.chdir(working_directory)

    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host=host, credentials=pika.PlainCredentials('adi', '24adna')))  # loopback address
    channel = connection.channel()

    channel.queue_declare(queue=queue_name)

    processThread = threading.Thread(target=get_q_size, args=(channel, queue_name,))
    processThread.start()

    # channel.basic_publish(exchange='', routing_key='hello', body='Hello World!')
    # print(" [x] Sent 'Hello World!'")
    # connection.close()
    file_as_bytes = None
    while True:
        # print('Checkinf for new files..')
        files = os.listdir()
        for file in files:
            if file != '.gitkeep':
                # print(f'Found file, reading bytes {file}')
                time.sleep(3)
                txt.write(f'{get_now_time()} ATTEMPT TO UPLOAD {file} to {queue_name} from dir {working_directory}\n')
                txt.flush()
                if enough_memory():
                    txt.write(f'{get_now_time()} ENOUGH MEM {file}\n')
                    txt.flush()
                    with open(file, "rb") as f:
                        file_as_bytes = f.read()

                        file_to_send = base64.b64encode(file_as_bytes).decode('utf-8')
                        # base64.encode(file_as_bytes, file_as_bytes)
                        # encoded = base64.b64encode(file_as_bytes)
                        txt.write(f'{get_now_time()}  PUBLISHING {file}\n')
                        txt.flush()
                        channel.basic_publish(exchange='',
                                              routing_key=queue_name,
                                              body=json.dumps({'file_name': file, 'binary': file_to_send}))
                        txt.write(f'{get_now_time()} PUBLISHED {file}\n')
                        txt.flush()

                    # print(" [x] Sent file to queue")
                    txt.write(f'{get_now_time()}  REMOVE {file}\n')
                    txt.flush()

                    os.remove(file)
                    txt.write(f'===============================================\n')
                    txt.flush()
                    # connection.close()
        time.sleep(1)


if __name__ == '__main__':
    host = sys.argv[1]
    second_argument = None
    third_argument = None
    try:
        third_argument = sys.argv[3]
    except IndexError:
        # Argument has not been provided
        pass
    try:
        second_argument = sys.argv[2]
    except IndexError:
        # Argument has not been provided
        pass

    global txt
    txt = open('producer_output.txt', 'a')
    try:
        work(host, second_argument, third_argument)
    except Exception as e:
        txt.write(f'EXCEPTION OCCURED {e}\n')
        txt.flush()
        time.sleep(1)
    txt.close()
