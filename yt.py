from pytube import Playlist
import os


# CHecking changes
def download_videos():
    playlist = Playlist('https://www.youtube.com/playlist?list=PL8gp3950ovA_I29EGM9_pzOIAjf1lHvPA')
    nr_videos = len(playlist.video_urls)
    print('Number of videos in playlist: %s' % nr_videos)

    # Loop through all videos in the playlist and download them
    current_video = 0
    for video in playlist.videos:
        current_video += 1
        print(f'Downloading video number {current_video}')
        video.streams.filter(progressive=True, file_extension='mp4').order_by('resolution').desc().first().download()


download_videos()
