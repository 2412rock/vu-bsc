import node_API
import time

split_node_ip = '10.149.21.1'
blur_node_ip = '10.149.21.2'
stabilize_video_node_ip = '10.149.21.3'
add_watermark_node_ip = '10.149.21.5'
reverse_node_ip = '10.149.21.18'
vintage_node_ip = '10.149.21.19'
merge_parts_node_ip = '10.149.21.4'

idle_node_1_ip = '10.149.21.7'
idle_node_2_ip = '10.149.21.8'
idle_node_3_ip = '10.149.21.9'
idle_node_4_ip = '10.149.21.10'
idle_node_5_ip = '10.149.21.11'
idle_node_6_ip = '10.149.21.12'
idle_node_7_ip = '10.149.21.13'
idle_node_8_ip = '10.149.21.14'
idle_node_9_ip = '10.149.21.15'
idle_node_10_ip = '10.149.21.16'


ssh_inst_1 = node_API.get_ssh_instance(split_node_ip, '24adna')
ssh_inst_2 = node_API.get_ssh_instance(blur_node_ip, '24adna')
ssh_inst_3 = node_API.get_ssh_instance(stabilize_video_node_ip, '24adna')
ssh_inst_4 = node_API.get_ssh_instance(add_watermark_node_ip, '24adna')
ssh_inst_5 = node_API.get_ssh_instance(reverse_node_ip, '24adna')
ssh_inst_6 = node_API.get_ssh_instance(vintage_node_ip, '24adna')
ssh_inst_7 = node_API.get_ssh_instance(merge_parts_node_ip, '24adna')

ssh_inst_8 = node_API.get_ssh_instance(idle_node_1_ip, '24adna')
ssh_inst_9 = node_API.get_ssh_instance(idle_node_2_ip, '24adna')
ssh_inst_10 = node_API.get_ssh_instance(idle_node_3_ip, '24adna')
ssh_inst_11 = node_API.get_ssh_instance(idle_node_4_ip, '24adna')
ssh_inst_12 = node_API.get_ssh_instance(idle_node_5_ip, '24adna')
ssh_inst_13 = node_API.get_ssh_instance(idle_node_6_ip, '24adna')
ssh_inst_14 = node_API.get_ssh_instance(idle_node_7_ip, '24adna')
ssh_inst_15 = node_API.get_ssh_instance(idle_node_8_ip, '24adna')
ssh_inst_16 = node_API.get_ssh_instance(idle_node_9_ip, '24adna')
ssh_inst_17 = node_API.get_ssh_instance(idle_node_10_ip, '24adna')

print('-------------------- NODE 1 ---------------------')
stdin, stdout, stderr = ssh_inst_1.exec_command(f'cat /home/template/code/vu-bsc/track_time.txt')
out = stdout.read().decode('utf-8').strip("\n")
print(out)
print('-------------------- NODE 2 ---------------------')
stdin, stdout, stderr = ssh_inst_2.exec_command(f'cat /home/template/code/vu-bsc/track_time.txt')
out = stdout.read().decode('utf-8').strip("\n")
print(out)
print('-------------------- NODE 3 ---------------------')
stdin, stdout, stderr = ssh_inst_3.exec_command(f'cat /home/template/code/vu-bsc/track_time.txt')
out = stdout.read().decode('utf-8').strip("\n")
print(out)
print('-------------------- NODE 4 ---------------------')
stdin, stdout, stderr = ssh_inst_4.exec_command(f'cat /home/template/code/vu-bsc/track_time.txt')
out = stdout.read().decode('utf-8').strip("\n")
print(out)
print('-------------------- NODE 5 ---------------------')
stdin, stdout, stderr = ssh_inst_5.exec_command(f'cat /home/template/code/vu-bsc/track_time.txt')
out = stdout.read().decode('utf-8').strip("\n")
print(out)
print('-------------------- NODE 6 ---------------------')
stdin, stdout, stderr = ssh_inst_6.exec_command(f'cat /home/template/code/vu-bsc/track_time.txt')
out = stdout.read().decode('utf-8').strip("\n")
print(out)
print('-------------------- NODE 7 ---------------------')
stdin, stdout, stderr = ssh_inst_7.exec_command(f'cat /home/template/code/vu-bsc/track_time.txt')
out = stdout.read().decode('utf-8').strip("\n")
print(out)
print('-------------------- NODE 10 ---------------------')
stdin, stdout, stderr = ssh_inst_8.exec_command(f'cat /home/template/code/vu-bsc/track_time.txt')
out = stdout.read().decode('utf-8').strip("\n")
print(out)
print('-------------------- NODE 11 ---------------------')
stdin, stdout, stderr = ssh_inst_9.exec_command(f'cat /home/template/code/vu-bsc/track_time.txt')
out = stdout.read().decode('utf-8').strip("\n")
print(out)
print('-------------------- NODE 12 ---------------------')
stdin, stdout, stderr = ssh_inst_10.exec_command(f'cat /home/template/code/vu-bsc/track_time.txt')
out = stdout.read().decode('utf-8').strip("\n")
print(out)
print('-------------------- NODE 13 ---------------------')
stdin, stdout, stderr = ssh_inst_11.exec_command(f'cat /home/template/code/vu-bsc/track_time.txt')
out = stdout.read().decode('utf-8').strip("\n")
print(out)
print('=========================== END ============================')
print('-------------------- NODE 14 ---------------------')
stdin, stdout, stderr = ssh_inst_12.exec_command(f'cat /home/template/code/vu-bsc/track_time.txt')
out = stdout.read().decode('utf-8').strip("\n")
print(out)
print('=========================== END ============================')
print('-------------------- NODE 15 ---------------------')
stdin, stdout, stderr = ssh_inst_13.exec_command(f'cat /home/template/code/vu-bsc/track_time.txt')
out = stdout.read().decode('utf-8').strip("\n")
print(out)
print('=========================== END ============================')
print('-------------------- NODE 16 ---------------------')
stdin, stdout, stderr = ssh_inst_14.exec_command(f'cat /home/template/code/vu-bsc/track_time.txt')
out = stdout.read().decode('utf-8').strip("\n")
print(out)
print('=========================== END ============================')
print('-------------------- NODE 17 ---------------------')
stdin, stdout, stderr = ssh_inst_15.exec_command(f'cat /home/template/code/vu-bsc/track_time.txt')
out = stdout.read().decode('utf-8').strip("\n")
print(out)
print('=========================== END ============================')
print('-------------------- NODE 18 ---------------------')
stdin, stdout, stderr = ssh_inst_16.exec_command(f'cat /home/template/code/vu-bsc/track_time.txt')
out = stdout.read().decode('utf-8').strip("\n")
print(out)
print('=========================== END ============================')
print('-------------------- NODE 19 ---------------------')
stdin, stdout, stderr = ssh_inst_17.exec_command(f'cat /home/template/code/vu-bsc/track_time.txt')
out = stdout.read().decode('utf-8').strip("\n")
print(out)
print('=========================== END ============================')
