import socket
import sys, os
from ilock import ILock, ILockException

host = sys.argv[1]
s = socket.socket()
s.bind((host, 65535))
s.listen(10) # Accepts up to 10 connections.

file_name = ''

def increment_q_size():
    count = 0
    try:
        f = open('first_q_size.txt', 'r')
        q_size = f.read()
        count = int(q_size)
        f.close()
    except:
        pass
    count += 1
    f = open('first_q_size.txt', 'w')
    f.write(str(count))
    f.close()


while True:
    _socket, address = s.accept()
    print('Listening')
    file_name = _socket.recv(1024).decode()
    #print(f'Received file name {file_name}')
    with ILock('q_update_lock', timeout=100):
        increment_q_size()
    _socket.send("Filename received.".encode('utf-8'))
    f = open(f'{file_name}', 'wb')
    file_data = _socket.recv(1024)
    #print('Received chunk')
    while file_data:
        f.write(file_data)
        file_data = _socket.recv(1024)
        #print('Received chunk')
    f.close()
    os.system(f'mv {file_name} distributed_files/{file_name}')



