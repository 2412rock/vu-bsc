import subprocess
import re


def queue_size(queue_name=None):
    if not queue_name:
        queue_name = 'hello'
    cmd = ['rabbitmqctl', 'list_queues']
    output = subprocess.Popen(cmd, stdout=subprocess.PIPE).communicate()[0]
    # print (output)
    output = output.decode('utf-8')
    # print(output)
    lines = output.splitlines()
    for line in lines:
        match = re.search(f"{queue_name}	(\d+)", line)
        if match:
            return (match.group(1))

    return -1

def queue_size_all(queue_names):
    return_values = []
    cmd = ['rabbitmqctl', 'list_queues']
    output = subprocess.Popen(cmd, stdout=subprocess.PIPE).communicate()[0]
    # print (output)
    output = output.decode('utf-8')
    # print(output)
    lines = output.splitlines()
    for line in lines:
        for que_name in queue_names:
            match = re.search(f"{que_name}	(\d+)", line)
            if match:
                return_values.append((match.group(1)))

    return return_values

# queue_size()

