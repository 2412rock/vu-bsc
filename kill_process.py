import os, signal
from datetime import datetime
import sys


def kill_process_name(name):
    try:
        # iterating through each instance of the process
        for line in os.popen('ps ax | grep "' + name + '" | grep -v grep'):
            fields = line.split()

            # extracting Process ID from the output
            pid = fields[0]

            # terminating process
            os.kill(int(pid), signal.SIGKILL)
        print("Process Successfully terminated")

    except:
        print("Error Encountered while running script")


if __name__ == '__main__':
    process_name = sys.argv[1]
    print(f'Killing process name "{process_name}"')
    # datetime object containing current date and time
    # now = datetime.now()
    #
    # # dd/mm/YY H:M:S
    # dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    # f = open('kill_consumer_output.txt', 'a')
    # f.write(f'{dt_string}  KILLING EVERYTHING\n')
    # f.close()
    kill_process_name(process_name)
    # #kill_process_name('socket_client.py')
    # kill_process_name('ffmpeg')
    # kill_process_name('consumer.py')
