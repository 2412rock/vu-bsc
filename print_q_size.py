import get_queue_size, sys

if __name__ == '__main__':
    queue_name = None
    try:
        queue_name = sys.argv[1]
    except IndexError:
        pass
    print(get_queue_size.queue_size(queue_name))
