import os, subprocess
import time


def launch_process(line):
    words = line.split()
    arr = []
    for word in words:
        arr.append(word)
    # print(f'Array consumer : {arr}')
    subprocess.Popen(arr)


def launch_socket_client_process():
    try:
        f = open('socket_client.txt', 'r')
        line = f.readline()
        arr = []
        words = line.split()
        for word in words:
            arr.append(word)
        # print(f'Array producer : {arr}')
        subprocess.Popen(arr)

    except Exception as e:
    # print(f'Exception at producer {e}')
        pass


def launch_merge_parts_process():
    try:
        f = open('merge_parts.txt', 'r')
        line = f.readline()
        arr = []
        words = line.split()
        for word in words:
            arr.append(word)
        # print(f'Array producer : {arr}')
        subprocess.Popen(arr)

    except Exception as e:
    # print(f'Exception at producer {e}')
        pass


def checker():
    cmd = subprocess.check_output("ps -fA | grep python3", shell=True);
    # print (output)
    running_python_processes = cmd.decode('utf-8')
    try:
        # multiple instances of consumer, not just one
        locked_flag_exists = os.path.exists('locked_flag')

        if not locked_flag_exists:
            # If locked flag exists, do not revive process which has exited
            f = open('consumer.txt', 'r')
            lines = f.readlines()
            for line in lines:
                if line not in running_python_processes:
                    # consumer process for some lo not active
                    # print('consumer process not running')
                    launch_process(line)
    except:
        pass
    # try:
    #     # multiple instances of producer, not just one
    #     f = open('producer.txt', 'r')
    #     lines = f.readlines()
    #     for line in lines:
    #         if line not in running_python_processes: # line not already running
    #             # producer process for some lo not active
    #             # print('producer process not running')
    #             launch_process(line)
    # except:
    #     pass
    # if 'socket_client.py' not in running_python_processes:
    #     # print('producer process not running')
    #     launch_socket_client_process()
    if 'merge_parts.py' not in running_python_processes:
        # print('producer process not running')
        launch_merge_parts_process()
    # print(output)
    # print(output)


if __name__ == '__main__':
    try:
        while True:
            checker()
            time.sleep(5)
    except Exception as e:
        f = open('checker_daemon_except', 'a')
        f.write(e)
        f.flush()
        f.close()
