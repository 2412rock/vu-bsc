import requests
import json
import urllib3
import re
import time

base_url = "https://192.168.1.236:7666/api/"
username = 'admin'
password = '24adnaA!'
valid_cert = False
auto_vm_prefix = 'aut_'
appformat = 'application/vnd.vmware.vmw.rest-v1+json'
headers = {'content-type': appformat, 'accept': appformat}
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


def get_vm_id(vm_name):
    print("Getting vm id for: " + vm_name)
    uri = 'vms'
    try:
        r = requests.get(base_url + uri, headers=headers, auth=(username, password), verify=valid_cert)
        print("Request status code -> " + str(r.status_code))
        if r.status_code == 200:
            response = json.dumps(r.json())
            data = json.loads(response)
            for i in data:
                if vm_name + '.vmx' in i['path']:
                    id = i['id']
                    return id
    except:
        print("Failed to get vm id")
    return -1


def create_copy_of_vm(parent_id, name):
    print("Creating copy of vm.." + parent_id)
    uri = 'vms'
    data = {
        "name": name,
        "parentId": parent_id
    }
    try:
        r = requests.post(base_url + uri, headers=headers, auth=(username, password), verify=valid_cert,
                          data=json.dumps(data))
        print("Request status code -> " + str(r.status_code))
        return r.status_code
    except:
        print('Failed to create copy of vm')
    return -1


def get_vm_ip(vm_id):
    print("Getting ip address for vm.." + vm_id)
    uri = 'vms/{0}/ip'.format(vm_id)
    while True:
        try:
            r = requests.get(base_url + uri, headers=headers, auth=(username, password), verify=valid_cert)
            print("Request status code -> " + str(r.status_code))
            if r.status_code == 200:
                response = json.dumps(r.json())
                data = json.loads(response)
                return data['ip']
            else:
                print("VM is still booting...retrying in 4 seconds")
                time.sleep(4)
        except:
            print('Failed to get vm ip')


def power_vm(vm_id, power_state):
    print('Powering {1} vm {0}..'.format(vm_id, power_state))
    uri = 'vms/{0}/power'.format(vm_id)
    try:
        r = requests.put(base_url + uri, headers=headers, auth=(username, password), verify=valid_cert,
                         data=power_state)
        print("Request status code -> " + str(r.status_code))
    except:
        print(f'Failed to power {power_state} vm')


def delete_vm(vm_number):
    vm_id = get_vm_id(f'aut_{vm_number}')
    print(f'Deleting vm id {vm_id}')
    uri = f'vms/{vm_id}'
    try:
        r = requests.delete(base_url + uri, auth=(username, password), verify=valid_cert)
        print(f'Request status code ->  {str(r.status_code)}')

    except Exception as e:
        print(f'Delete vm failed with exception {e}')


def turn_off_vm(vm_number):
    print('Turning off vm ' + vm_number)
    new_vm_id = get_vm_id('aut_' + vm_number)
    power_vm(new_vm_id, 'off')


def turn_on_vm(vm_number):
    print('Turning on vm ' + vm_number)
    new_vm_id = None
    if vm_number == 'setup_vm':
        new_vm_id = get_vm_id(vm_number)
    else:
        new_vm_id = get_vm_id('aut_' + vm_number)
    power_vm(new_vm_id, 'on')


def turn_on_vm_and_get_ip(vm_number):
    print('Turning on vm ' + vm_number + ' and return ip')
    new_vm_id = None
    if vm_number == 'setup_vm':
        new_vm_id = get_vm_id('setup_vm')
    else:
        new_vm_id = get_vm_id('aut_' + vm_number)
    power_vm(new_vm_id, 'on')
    ip = get_vm_ip(new_vm_id)
    return ip


def create_new_VM(vm_internal_id):
    print("Creating new vm...")
    id_of_template_vm = get_vm_id("template_vm")
    status = create_copy_of_vm(id_of_template_vm, 'aut_' + vm_internal_id)
    if status == 200 or status == 201 or status == 409:
        new_vm_id = get_vm_id('aut_' + vm_internal_id)
        power_vm(new_vm_id, 'on')
        ip = get_vm_ip(new_vm_id)
        print("VM boot successful, ip:" + ip)
        return ip
    else:
        return -1
